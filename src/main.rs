// This is an example of how the Solver crate can be used

use std::collections::HashSet;
use constraints::Solver;

fn main() {
    // We are trying to solve a 20x20 grid
    let input_space = (0 as usize..20 as usize).map(|row| (0 as usize..20 as usize).map(move |col| (row, col))).flatten();
    // Each cell of the grid can take one of the following characters
    // | represents a flagpole
    // > represents a flag
    // █ represents a platform
    // the blank space character represents air
    let output_space = ['|', '>', '█', ' '].iter();
    // This closure describes which characters can be adjacent to each other
    let compute_neighbor_constraints = |i: &(usize, usize), o: &char| {
        match o {
            '|' => {
                let mut v = vec![];
                match i.1.checked_add(1) {
                    // For instance, a flagpole block can either have more flagpole above it, or a flag
                    Some(val) => v.push(((i.0, val), ['>','|'].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                match i.0.checked_add(1) {
                    // On the other hand, a flagpole can only have air on either side of it
                    Some(val) => v.push(((val, i.1), [' '].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                match i.1.checked_sub(1) {
                    // a flagpole can have a flagpole below it, or a platform
                    Some(val) => v.push(((i.0, val), ['█','|'].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                match i.0.checked_sub(1) {
                    Some(val) => v.push(((val, i.1), [' '].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                v
            },
            // and so on...
            '>' => {
                let mut v = vec![];
                match i.1.checked_add(1) {
                    Some(val) => v.push(((i.0, val), ['█',' '].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                match i.0.checked_add(1) {
                    Some(val) => v.push(((val, i.1), [' '].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                match i.1.checked_sub(1) {
                    Some(val) => v.push(((i.0, val), ['|'].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                match i.0.checked_sub(1) {
                    Some(val) => v.push(((val, i.1), [' '].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                v
            },
            '█' => {
                let mut v = vec![];
                match i.1.checked_add(1) {
                    Some(val) => {
                        v.push(((i.0, val), ['|',' ',].iter().collect::<HashSet<&char>>()));
                        match i.0.checked_add(1) {
                            Some(val2) => v.push(((val2, val), [' ','|','>'].iter().collect::<HashSet<&char>>())),
                            None => ()
                        }
                        match i.0.checked_sub(1) {
                            Some(val2) => v.push(((val2, val), [' ','|','>'].iter().collect::<HashSet<&char>>())),
                            None => ()
                        }
                    },
                    None => ()
                }
                match i.0.checked_add(1) {
                    Some(val) => v.push(((val, i.1), ['█',' ',].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                match i.1.checked_sub(1) {
                    Some(val) => {
                        v.push(((i.0, val), ['>',' ',].iter().collect::<HashSet<&char>>()));
                        match i.0.checked_add(1) {
                            Some(val2) => v.push(((val2, val), [' ','|','>'].iter().collect::<HashSet<&char>>())),
                            None => ()
                        }
                        match i.0.checked_sub(1) {
                            Some(val2) => v.push(((val2, val), [' ','|','>'].iter().collect::<HashSet<&char>>())),
                            None => ()
                        }
                    },
                    None => ()
                }
                match i.0.checked_sub(1) {
                    Some(val) => v.push(((val, i.1), ['█',' '].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                v
            },
            ' ' => {
                let mut v = vec![];
                match i.1.checked_add(1) {
                    Some(val) => v.push(((i.0, val), ['█',' '].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                match i.0.checked_add(1) {
                    Some(val) => v.push(((val, i.1), ['|','>','█',' '].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                match i.1.checked_sub(1) {
                    Some(val) => v.push(((i.0, val), ['|','>','█',' '].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                match i.0.checked_sub(1) {
                    Some(val) => v.push(((val, i.1), ['|','>','█',' '].iter().collect::<HashSet<&char>>())),
                    None => ()
                }
                v
            },
            _ => panic!()
        }.into_iter()
    };
    
    // We create a solver with the given input_space, output_space, and constraint function
    let mut solver = Solver::new(input_space, output_space, compute_neighbor_constraints);
    // we preemptively collapse some of our cells; for instance, we don't want any flagpoles
    // or flags at the bottom of the grid, so we collapse all the cells along the bottom so that
    // they can only contain platforms or air
    for i in 0..20 {
        solver.collapse(&(i, 0), ['█', ' '].iter());
        // similarly, we don't want platforms or flagpoles at the top, so we collapse all the
        // cells along the top so that they can only contain air or flags.
        solver.collapse(&(i, 19), [' ', '>'].iter());
    }
    // Then, we solve our constraints
    let solver = solver.solve().unwrap();
    // and print the result
    for i in (0..20).rev() {
        for j in 0..20 {
            print!("{}", solver[&(j, i)].clone().into_iter().next().unwrap());
        }
        println!();
    }
}