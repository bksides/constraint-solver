// This library implements a type of constraint solver.

use std::hash::Hash;
use std::collections::{HashSet, HashMap};
use std::ops::{ Index, IndexMut };
use std::iter::{ FromIterator, once };
use std::fmt::Debug;

#[derive(Clone)]
pub struct Solver<I: Eq + Hash + Clone, O: Clone + Eq + Hash, C: Iterator<Item=(I, HashSet<O>)> + Clone, F: Fn(&I, O) -> C + Clone> {
    // map from elements of the input space to their possible values
    domain: HashMap<I, HashSet<O>>,
    // quick way to get cells with a specified entropy, i.e. a specified number of possible values
    entropy_map: Vec<HashSet<I>>,
    // calculates constraints on cells given that a particular cell has a particular value
    compute_neighbor_constraints: F
}

impl<I: Eq + Hash + Clone, O: Clone + Eq + Hash, C: Iterator<Item=(I, HashSet<O>)> + Clone, F: Fn(&I, O) -> C + Clone> Index<&I> for Solver<I, O, C, F> {
    type Output = HashSet<O>;

    fn index(&self, ind: &I) -> &Self::Output {
        return self.domain.get(&ind).unwrap();
    }
}

impl<I: Eq + Hash + Clone, O: Clone + Eq + Hash, C: Iterator<Item=(I, HashSet<O>)> + Clone, F: Fn(&I, O) -> C + Clone> IndexMut<&I> for Solver<I, O, C, F> {
    fn index_mut(&mut self, ind: &I) -> &mut Self::Output {
        return self.domain.get_mut(&ind).unwrap();
    }
}

impl<I: Debug + Eq + Hash + Clone, O: Clone + Eq + Hash + Debug, C: Iterator<Item=(I, HashSet<O>)> + Clone, F: Fn(&I, O) -> C + Clone> Solver<I, O, C, F> {
    // Create a new solver for the given input space and output space, where the constraints are calculated using compute_neighbor_constraints
    pub fn new<I1: IntoIterator<Item=I>, I2: IntoIterator<Item=O>>(input_space: I1, output_space: I2, compute_neighbor_constraints: F) -> Self {
        let output_space: Vec<O> = output_space.into_iter().collect();
        let input_space: Vec<I> = input_space.into_iter().collect();

        let mut domain = HashMap::<I, HashSet<O>>::new();
        input_space.into_iter().for_each(|i| {
            domain.insert(i, HashSet::from_iter(output_space.iter().map(|o| o.clone())));
        });

        let mut entropy_map = vec![HashSet::<I>::new(); output_space.into_iter().count()+1];
        domain.keys().for_each(|i| {
            let entropy = domain[i].len();
            entropy_map[entropy].insert(i.clone());
        });

        return Solver{
            domain,
            entropy_map,
            compute_neighbor_constraints
        };
    }

    // Force the value at i to take one of the values in possible_values
    pub fn collapse<OIter: IntoIterator<Item=O> + Debug>(&mut self, i: &I, possible_values: OIter) {
        if !(self.domain.contains_key(i)) {
            return
        }

        let current_set = self[i].clone();
        let i_entropy = current_set.len();
        self.entropy_map[i_entropy].remove(i);
        self[i] = HashSet::from_iter(possible_values).intersection(&current_set).map(|o| o.clone()).collect();
        let new_entropy = self[i].len();
        self.entropy_map[i_entropy].remove(i);
        if new_entropy != 1 {
            self.entropy_map[new_entropy].insert(i.clone());
        }

        if i_entropy != new_entropy {
            let mut neighbor_constraints = HashMap::<I, HashSet<O>>::new();
            self[i].clone().into_iter().for_each(|o| {
                let constraints = (self.compute_neighbor_constraints)(i, o.clone());
                constraints.into_iter().for_each(| (j, o_set) | {
                    match neighbor_constraints.get_mut(&j) {
                        Some(set) => set.extend(o_set),
                        None => { neighbor_constraints.insert(j.clone(), o_set); }
                    }
                });
            });
            neighbor_constraints.into_iter().for_each(|(neighbor, constraints)| self.collapse(&neighbor, constraints));
        }
    }

    // Find a solution for the solver, if one exists
    pub fn solve(self) -> Option<Self> {
        // try to get the lowest non-empty entropy level
        // the "entropy level" corresponds to how many possible values the cell could have
        // lower entropy cells have fewer possibilities which means we're less likely to
        // pick a value that will lead to a contradiction
        match self.entropy_map.iter().enumerate().find(|(_, cells)| !cells.is_empty()).map(|result| result.clone()) {
            // if we find a non-empty entropy level, then:
            Some((_, cells)) => {
                // get a cell from that entropy level to collapse
                let next_i_to_collapse = cells.into_iter().next().unwrap();
                // map each possible value for the cell to a potential solution
                self[next_i_to_collapse].iter().map(|possible_element| {
                    let mut collapsed = self.clone();
                    // by collapsing the cell in a copy of this solver
                    collapsed.collapse(next_i_to_collapse, HashSet::<O>::from_iter(once(possible_element.clone().clone())));
                    // and then solving the copy
                    collapsed.solve()
                }).find(|solution| solution.is_some()).flatten() // we return the first valid solution
            },
            // if there are no non-empty entropy levels, that means all cells are completely solved and thus, we're done solving
            // so we just return the given solver
            None => Some(self)
        }
    }
}