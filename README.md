# Constraint Solver

This is a simple constraint solver that can be applied to a variety of domains and output spaces.

## Usage

You can create a new solver with `Solver::new(input_space, output_space,
constraint_function)`, where the parameters are as follows:
- `input_space`: an object that [can be coerced into an
    iterator](https://doc.rust-lang.org/std/iter/trait.IntoIterator.html)
    over the domain of the solver. For example, if you were trying to
    generate a 10x10 grid, your input space might be an iterator over all
    2-tuplesfrom `(0, 0)` to `(10, 10)`.

- `output_space`: an object that [can be coerced into an
    iterator](https://doc.rust-lang.org/std/iter/trait.IntoIterator.html) 
    over the range of the solver. For example, if each cell of your grid
    could have a value of `'a'`, `'b'`, or `'c'`, your input space might be
    an iterator over those values.

- `constraint_function`: A [function](https://doc.rust-lang.org/std/ops/trait.Fn.html) 
of the form `(i, o) -> c` where:
    - `i` is an element of the input space
    - `o` is an element of the output space
    - `c` is an iterator whose items are tuples.  The first item of each tuple is
        an element of the input space, and the second item of each tuple is a
        [HashSet](https://doc.rust-lang.org/std/collections/struct.HashSet.html)
        of elements of the output set which would be permissible if the value
        of `i` were set to `o`.

You can call `solver.collapse(i, s)`, where `i` is an element of the input space and
`s` is an object that [can be coerced into an
iterator](https://doc.rust-lang.org/std/iter/trait.IntoIterator.html) over some set
of elements in the output space.  The value associated with `i` will then be forced
to take the value of some element in `s`.

Finally, `solver.solve()` will return a valid solution to the constraints, given
any inputs you have already collapsed.